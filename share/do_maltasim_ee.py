#!/usr/bin/env python
import os
import ROOT
import Experiment
import step
import maltadata
import random
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("-o","--output",default="do_maltasim_ee.root")
args=parser.parse_args()

print("Create the experiment")
exp=Experiment.Experiment()
exp.setVerbose(args.verbose)

print("Create histograms")
hTruePos = ROOT.TH2D("hTruePos",";Pixel Col; Pixel Row",32,0,32,32,0,32)
hWordPos = ROOT.TH2D("hWordPos",";Pixel Col; Pixel Row",32,0,32,32,0,32)
hRecoPos = ROOT.TH2D("hRecoPos",";Pixel Col; Pixel Row",32,0,32,32,0,32)

print("Loop over events")
for evt in range(2000):
	if args.verbose: print("Event: %5i" % evt)
	# generate one event
	exp.clear()
	stp = step.Step()
	sx=random.uniform(0,32)
	sy=random.uniform(0,32)
	stp.setXYZ0(sx*36.4,sy*36.4,0)
	stp.setEdep(1000)
	exp.addStep(stp)
	hTruePos.Fill(sx,sy)
	# start simulation
	exp.startOfEvent()
	exp.endOfEvent()
    # use the reconstruction
	for digit in exp.getDigits():
		md=maltadata.MaltaData()
		digit.pack()
		md.setBitStream(digit.getBitStream())
		for i in range(md.getNhits()):
			hRecoPos.Fill(md.getHitCol(i),md.getHitRow(i))
			pass
		for word in digit.getWords():
			hWordPos.Fill(word.getCol(),word.getRow())
			pass
		pass
	pass
    
ROOT.gROOT.SetStyle("ATLAS")

print("Open file: %s" % args.output)
fw=ROOT.TFile(args.output,"RECREATE")
c1=ROOT.TCanvas("c1","",400*3,400)
c1.Divide(3,1)
tt=ROOT.TLatex()
c1.cd(1)
hTruePos.Draw("COLZ")
hTruePos.Write()
c1.cd(2)
hWordPos.Draw("COLZ")
hWordPos.Write()
c1.cd(3)
hRecoPos.Draw("COLZ")
hRecoPos.Write()
  
c1.Update()
fw.Close()

