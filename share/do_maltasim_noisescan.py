#!/usr/bin/env python
import os
import ROOT
import Experiment
import step
import random
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("-n","--num-events",help="number of events per charge",type=int,default=100)
parser.add_argument("-idb","--idb",help="IDB",type=int,default=120)
parser.add_argument("-ithr","--ithr",help="ITHR",type=int,default=60)
parser.add_argument("-ibias","--ibias",help="IBIAS",type=int,default=43)
parser.add_argument("-icasn","--icasn",help="ICASN",type=int,default=10)
parser.add_argument("-o","--output",default="do_maltasim_noisescan.root")
args=parser.parse_args()

# initialize simulation
exp=Experiment.Experiment()
exp.setVerbose(args.verbose)
# set the pixel threshold and noise
exp.getMalta().getPixel().setDACs(args.idb,args.ithr,args.ibias,args.icasn)
# extract pixel quantities
pix_sx=exp.getMalta().getPixel().getColSize() 
pix_sy=exp.getMalta().getPixel().getRowSize()
# get the number of rows and columns
ncols=exp.getMalta().getNcols()
nrows=exp.getMalta().getNrows()

print("Simulate a noise scan")

# create the occupancy histogram
hHitsVsRowCol = ROOT.TH2F("hHitsVsRowCol",";Col;Row;Events",ncols,0,ncols,nrows,0,nrows)

# loop events
charge = exp.getMalta().getPixel().getThreshold()
for evt in range(0,args.num_events):
	print("Event: %i"%evt)
	# define the injection in the pixel
	exp.clear()
	for col in range(ncols):
		for row in range(nrows):
			if random.randint(0,10000)>0: continue
			stp = step.Step()
			sx=col*pix_sx+pix_sx/2.
			sy=row*pix_sy+pix_sy/2.
			stp.setXYZ0(sx,sy,0)
			stp.setEdep(charge)
			exp.addStep(stp)
		pass
	pass
	# start simulation
	exp.startOfEvent()
	exp.endOfEvent()  
	# get the hits
	nhits=0
	for digit in exp.getDigits():
		for word in digit.getWords():
			for hit in word.getHits():
				if args.verbose: print("Hit: %s" % hit.toStr())
				nhits+=1
				hHitsVsRowCol.Fill(hit.getCol(),hit.getRow(),1)
				pass
			pass
		pass
	print("number of hits: %i" % nhits)
	pass
	
ROOT.gROOT.SetStyle("ATLAS")

print("Open file: %s" % args.output)
fw=ROOT.TFile(args.output,"RECREATE")
c1=ROOT.TCanvas("c1","",400,400)
tt=ROOT.TLatex()
hHitsVsRowCol.Draw("COLZ")

hHitsVsRowCol.Write()
c1.Update()
c1.SaveAs("do_maltasim_noisescan_c1.pdf")
fw.Close()

