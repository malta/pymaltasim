#!/usr/bin/env python
import os
import ROOT
import Experiment
import step
import random
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("-r","--row",help="pixel row",type=int,default=0)
parser.add_argument("-c","--col",help="pixel column",type=int,default=0)
parser.add_argument("-q","--min-charge",help="minimum charge in electrons",type=float,default=200)
parser.add_argument("-Q","--max-charge",help="maximum charge in electrons",type=float,default=400)
parser.add_argument("-s","--stp-charge",help="charge step in electrons",type=float,default=30)
parser.add_argument("-n","--num-events",help="number of events per charge",type=int,default=100)
parser.add_argument("-idb","--idb",help="IDB",type=int,default=120)
parser.add_argument("-ithr","--ithr",help="ITHR",type=int,default=60)
parser.add_argument("-ibias","--ibias",help="IBIAS",type=int,default=43)
parser.add_argument("-icasn","--icasn",help="ICASN",type=int,default=10)
parser.add_argument("-o","--output",default="do_maltasim_thscan.root")
args=parser.parse_args()

# initialize simulation
exp=Experiment.Experiment()
exp.setVerbose(args.verbose)
# set the pixel threshold and noise
#exp.getMalta().getPixel().setThreshold(300)
#exp.getMalta().getPixel().setNoise(10)
exp.getMalta().getPixel().setDACs(args.idb,args.ithr,args.ibias,args.icasn)
# extract pixel quantities
pix_sx=exp.getMalta().getPixel().getColSize() 
pix_sy=exp.getMalta().getPixel().getRowSize()
# calculate the number of steps
num_charge_steps=int((args.max_charge-args.min_charge)/float(args.stp_charge))+1

print("Simulate a threshold scan")
print("min_charge: %i" %args.min_charge)
print("max_charge: %i" %args.max_charge)
print("num_charge_steps: %i" % num_charge_steps)

# create the analysis histogram
#hHitsVsCharge = ROOT.TH1F("hHitsVsCharge",";Charge [electrons]; Events",num_charge_steps+1,args.min_charge-args.stp_charge/2.,args.max_charge+args.stp_charge/2.)
hHitsVsCharge = ROOT.TH1F("hHitsVsCharge",";Charge [electrons]; Events",num_charge_steps,args.min_charge,args.max_charge)

# loop charge
for cstep in range(0,num_charge_steps):
	charge = args.min_charge+args.stp_charge*cstep
	print("charge step: %i value: %.2f" % (cstep,charge))
	nhits=0
	for evt in range(0,args.num_events):
		# define the injection in the pixel
		exp.clear()
		stp = step.Step()
		sx=args.col*pix_sx+pix_sx/2.
		sy=args.row*pix_sy+pix_sy/2.
		stp.setXYZ0(sx,sy,0)
		stp.setEdep(charge)
		exp.addStep(stp)
		# start simulation
		exp.startOfEvent()
		exp.endOfEvent()  
		# get the hits
		for digit in exp.getDigits():
			for word in digit.getWords():
				for hit in word.getHits():
					if args.verbose: print("Hit: %s" % hit.toStr())
					nhits+=1
					if charge >= hHitsVsCharge.GetXaxis().GetXmax(): charge-=args.stp_charge/2.
					hHitsVsCharge.Fill(charge,1)
					pass
				pass
			pass
		pass
	print("number of hits: %i" % nhits)
	pass
	
ROOT.gROOT.SetStyle("ATLAS")

print("Open file: %s" % args.output)
fw=ROOT.TFile(args.output,"RECREATE")
c1=ROOT.TCanvas("c1","",400,400)
tt=ROOT.TLatex()
hHitsVsCharge.Draw()
# fit an s-curve to measure the threshold and the noise
f1=ROOT.TF1("scurve","[0]*0.5*(1+erf((x-[1])/[2]))");
f1.SetParameter(0,args.num_events)
f1.SetParameter(1,(args.max_charge-args.min_charge)*0.5+args.min_charge)
f1.SetParameter(2,1)
f1.SetParLimits(0,0,2*args.num_events)
f1.SetParLimits(1,args.min_charge,args.max_charge)
f1.SetParLimits(2,0,100)
hHitsVsCharge.Fit(f1)
# print the threshold and the noise
tt.DrawLatexNDC(0.2,0.85,"Thr: %i e"%f1.GetParameter(1))
tt.DrawLatexNDC(0.2,0.80,"Noise: %i e"%f1.GetParameter(2))
tt.DrawLatexNDC(0.2,0.75,"ExpThr: %i e"%exp.getMalta().getPixel().getThreshold())
tt.DrawLatexNDC(0.2,0.70,"ExpNoise: %i e"%exp.getMalta().getPixel().getNoise())

hHitsVsCharge.Write()

c1.Update()
c1.SaveAs("do_maltasim_thrscan_c1.pdf")
fw.Close()

