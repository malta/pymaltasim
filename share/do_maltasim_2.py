#!/usr/bin/env python
import os
import ROOT
import Experiment
import step
import random
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("-o","--output",default="output.root")
args=parser.parse_args()



exp=Experiment.Experiment()
exp.setVerbose(args.verbose)

pix_sx=exp.getMalta().getPixel().getColSize() #um
pix_sy=exp.getMalta().getPixel().getRowSize() #um
xmin=-pix_sx/2.
xmax=+pix_sx/2.
ymin=-pix_sy/2.
ymax=+pix_sy/2.

doses=[0,1,2,3]

hHitsVsPos = ROOT.TH2D("hHitsVsPos",";Pixel X [um]; Pixel Y [um]",20,xmin,xmax,20,ymin,ymax)
hTimeHitVsPos = []
for dose in doses:
  h2=ROOT.TProfile2D("hTimeHitVsPos_%i"%dose,";Pixel X [um]; Pixel Y [um]",20,xmin,xmax,20,ymin,ymax)
  hTimeHitVsPos.append(h2)
  pass

for binx in range(hHitsVsPos.GetNbinsX()):
  for biny in range(hHitsVsPos.GetNbinsY()):
    rx=hHitsVsPos.GetXaxis().GetBinCenter(binx+1)
    ry=hHitsVsPos.GetYaxis().GetBinCenter(biny+1)
    hHitsVsPos.Fill(rx,ry)
    hx=rx-xmin
    hy=ry-ymin
    if args.verbose: print("Hit x: %5.2f um, y: %5.2f um, rx: %5.2f, ry: %5.2f" % (hx,hy,rx,ry))
    for dose in doses:
      exp.getMalta().getPixel().setDose(dose)
      exp.clear()
      stp = step.Step()
      stp.setXYZ0(hx,hy,0)
      stp.setEdep(3000)
      exp.addStep(stp)
      # start simulation
      exp.startOfEvent()
      exp.endOfEvent()
  
      for digit in exp.getDigits():
        for word in digit.getWords():
          for hit in word.getHits():
            hTimeHitVsPos[dose].Fill(hit.getRX(),hit.getRY(),hit.getTime())
            pass
          pass
        pass
      pass
    pass
  pass

ROOT.gROOT.SetStyle("ATLAS")

print("Open file: %s" % args.output)
fw=ROOT.TFile(args.output,"RECREATE")
c1=ROOT.TCanvas("c1","",400*5,400)
c1.Divide(5,1)
	
c1.cd(1)
hHitsVsPos.Draw("COLZ")
hHitsVsPos.Write()

for dose in doses:
  c1.cd(dose+2)
  hTimeHitVsPos[dose].Draw("COLZ")
  hTimeHitVsPos[dose].Write()
  pass
  
c1.Update()
fw.Close()

