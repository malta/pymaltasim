#!/usr/bin/env python
import os
import ROOT
import Experiment
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-n","--numhits",help="max number of hits. default=50",default=50,type=int)
parser.add_argument("-e","--events",help="events per step. default=100",default=100,type=int)
parser.add_argument("-c","--charge",help="charge per step. default=300",default=300,type=int)
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("-o","--output",default="do_maltasim_1.root")
args=parser.parse_args()


exp=Experiment.Experiment()
exp.setVerbose(args.verbose)

hNDigitsVsNHits = ROOT.TH2D("hNDigitsVsNHits",";Number of hits; Number of Digits",args.numhits,1,args.numhits,20,1,20)
hNWordsPerDigitVsNHits = ROOT.TH2D("hNWordsPerDigitVsNHits",";Number of hits; Number of Words per Digits",args.numhits,1,args.numhits,20,1,20)

for numhits in range(1,args.numhits):
	for sim in range(args.events):
		exp.clear()
		exp.generateSteps(numhits,args.charge,0)
		exp.startOfEvent()
		exp.endOfEvent()
		numdigits=len(exp.getDigits())
		if args.verbose: print("NumHits: %i, NumDigits: %i" % (numhits, numdigits))
		hNDigitsVsNHits.Fill(numhits,numdigits)
		for digit in exp.getDigits():
			hNWordsPerDigitVsNHits.Fill(numhits,len(digit.getWords()))
			pass
		pass
	pass

ROOT.gROOT.SetStyle("ATLAS")
	
fw=ROOT.TFile(args.output,"RECREATE")
c1=ROOT.TCanvas("c1","",400*2,400)
c1.Divide(2,1)

c1.cd(1)
hNDigitsVsNHits.Draw("COLZ")
hNDigitsVsNHits.Write()

c1.cd(2)
hNWordsPerDigitVsNHits.Draw("COLZ")
hNWordsPerDigitVsNHits.Write()

c1.Update()
c1.Write()

c1.SaveAs("Digits.pdf")
fw.Close()

