#!/usr/bin/env python
import os
import ROOT
import Experiment
import step
import random
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-v","--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("-o","--output",default="do_maltasim_3.root")
args=parser.parse_args()

exp=Experiment.Experiment()
exp.setVerbose(args.verbose)

pix_sx=exp.getMalta().getPixel().getColSize() #um
pix_sy=exp.getMalta().getPixel().getRowSize() #um
xmin=0
xmax=4
ymin=0
ymax=8

patterns={
	"single":[[0,0],],
	"pair":[[0,0],[1,0]],
	"column":[[0,0],[0,1]],
	"cross-pair":[[1,0],[2,0]],
	"four":[[0,0],[0,1],[1,0],[1,1]],
	"cross-four":[[1,0],[2,0],[1,1],[2,1]],
	}

hHitsVsPos = {}
hTimeVsPos = {}

for pattern in patterns:
	if args.verbose: print("Processing new pattern: %s" % pattern)
	hHitsVsPos[pattern] = ROOT.TH2D("hHitsVsPos_%s"%pattern,";Pixel Col; Pixel Row",xmax-xmin,xmin,xmax,ymax-ymin,ymin,ymax)
	hTimeVsPos[pattern] = ROOT.TProfile2D("hTimeHitVsPos_%s"%pattern,";Pixel Col; Pixel Row",xmax-xmin,xmin,xmax,ymax-ymin,ymin,ymax)
	exp.clear()
	ene=1500./float(len(patterns[pattern]))
	for hit in patterns[pattern]:
		stp = step.Step()
		hx=hit[0]*pix_sx+pix_sx/2.
		hy=hit[1]*pix_sy+pix_sy/2.
		stp.setXYZ0(hx,hy,0)
		stp.setEdep(ene)
		exp.addStep(stp)
		hHitsVsPos[pattern].Fill(hit[0],hit[1])
		pass
	# start simulation
	exp.startOfEvent()
	exp.endOfEvent()
      
	for digit in exp.getDigits():
		for word in digit.getWords():
			for hit in word.getHits():
				if args.verbose: print("Hit: %s" % hit.toStr())
				hTimeVsPos[pattern].Fill(hit.getX(),hit.getY(),hit.getTime())
				pass
			pass
		pass
	pass
    
ROOT.gROOT.SetStyle("ATLAS")

print("Open file: %s" % args.output)
fw=ROOT.TFile(args.output,"RECREATE")
c1=ROOT.TCanvas("c1","",400*5,400)
c1.Divide(len(patterns),2)
tt=ROOT.TLatex()
i=0
for pattern in patterns:
	c1.cd(i+1)
	hHitsVsPos[pattern].Draw("COLZ")
	hHitsVsPos[pattern].Write()
	tt.DrawLatexNDC(0.3,0.9,"Hit pattern %s"%pattern)
	c1.cd(i+7)
	hTimeVsPos[pattern].Draw("COLZ")
	hTimeVsPos[pattern].Write()
	tt.DrawLatexNDC(0.3,0.9,"Time distrib %s"%pattern)
	i+=1
	pass
  
c1.Update()
fw.Close()

