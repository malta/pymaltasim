#!/usr/bin/env python

class MaltaData():
	def __init__(self):
		self.word1 = 0
		self.word2 = 0
		self.rows = []
		self.cols = []
		self.nhits = 0
		self.refbit = 0
		self.pixel = 0
		self.group = 0
		self.parity = 0
		self.delay = 0
		self.dcolumn = 0
		self.chipbcid = 0
		self.chipid = 0
		self.phase = 0
		self.winid = 0
		self.bcid = 0
		self.l1id = 0
		pass
	def clear(self):
		self.word1 = 0
		self.word2 = 0
		self.rows = []
		self.cols = []
		self.nhits = 0
		self.refbit = 0
		self.pixel = 0
		self.group = 0
		self.parity = 0
		self.delay = 0
		self.dcolumn = 0
		self.chipbcid = 0
		self.chipid = 0
		self.phase = 0
		self.winid = 0
		self.bcid = 0
		self.l1id = 0
		pass
	def setVerbose(self,enable):
		self.verbose=enable
		pass
	def getNhits(self):
		return self.nhits
	def getHitRow(self,hit):
		return self.rows[hit]
	def getHitCol(self,hit):
		return self.cols[hit]
	def setBitStream(self,bs):
		self.word1 = ( bs >>  0 ) & 0xFFFFFFFF
		self.word2 = ( bs >> 31 ) & 0xFFFFFFFF
		self.unpack()
		pass
	def setWord1(self,word):
		self.word1 = word
		pass
	def setWord2(self,word):
		self.word2 = word
		pass
	def unpack(self):
		self.refbit   = (self.word1 >>  0) & 0x1
		self.pixel    = (self.word1 >>  1) & 0xFFFF
		self.group    = (self.word1 >> 17) & 0x1F
		self.parity   = (self.word1 >> 22) & 0x1
		self.delay    = (self.word1 >> 23) & 0x7
		self.dcolumn  = (self.word1 >> 26) & 0x1F
		self.dcolumn |= ((self.word2 & 0x7) << 5)
		self.chipbcid = (self.word2 >> (34-31) & 0x3)
		self.chipid   = (self.word2 >> (36-31) & 0x1)
		self.phase    = (self.word2 >> (37-31) & 0x7)
		self.winid    = (self.word2 >> (40-31) & 0x7) 
		self.bcid     = (self.word2 >> (44-31) & 0x1F)
		self.l1id     = (self.word2 >> (50-31) & 0xFFF)
		#decode the hits
		self.nhits = 0
		self.rows = []
		self.cols = []
		for i in range(16):
			if ((self.pixel>>i)&0x1)==0: continue
			col = self.dcolumn*2
			if i>7: col+=1
			row = self.group*16
			if self.parity==1: row+=8
			if   i==0 or i== 8: row+=0
			elif i==1 or i== 9: row+=1
			elif i==2 or i==10: row+=2
			elif i==3 or i==11: row+=3
			elif i==4 or i==12: row+=4
			elif i==5 or i==13: row+=5
			elif i==6 or i==14: row+=6
			elif i==7 or i==15: row+=7
			self.rows.append(row)
			self.cols.append(col)
			self.nhits+=1
			pass
		pass
	def pack(self):
		self.word1=0;
		self.word2=0;
		self.word1 |= (self.refbit   & 0x1)    << 0
		self.word1 |= (self.pixel    & 0xFFFF) << 1
		self.word1 |= (self.group    & 0x1F)   << 17
		self.word1 |= (self.parity   & 0x1)    << 22
		self.word1 |= (self.delay    & 0x7)    << 23
		self.word1 |= (self.dcolumn  & 0x1F)   << 26
		self.word2 |= (self.dcolumn  >> 5) & 0x7
		self.word2 |= (self.chipbcid & 0x3)    << (34-31)
		self.word2 |= (self.chipid   & 0x1)    << (36-31)
		self.word2 |= (self.phase    & 0x7)    << (37-31)
		self.word2 |= (self.winid    & 0xF)    << (40-31)
		self.word2 |= (self.bcid     & 0x3F)   << (44-31)
		self.word2 |= (self.l1id     & 0xFFF)  << (50-31)
		pass	
	pass

