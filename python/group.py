#!/usr/bin/env python

from hit import Hit
from word import Word

class Group():
	
	#Create a group given the pixel coordinates
	def __init__(self,col,row):
		self.words = []
		self.ncols = 2
		self.nrows = 8
		self.col = col
		self.row = row
		self.rpg_delay_ns = 10
		pass
	def setRPGDelay(self,ns):
		self.rpg_delay_ns = ns
		pass
	def setCol(self,col):
		self.col=col
		pass
	def setRow(self,row):
		self.row=row
		pass
	def getCol(self):
		return self.col
	def getRow(self):
		return self.row
	def getNCols(self):
		return self.ncols
	def getNRows(self):
		return self.nrows
	def clear(self):
		self.words=[]
		pass
	def getWords(self):
		return self.words
	def processHits(self,hits):
		t0=9999
		selhits=[]
		for hit in hits:
			if hit.getX() >= self.col and hit.getX() < (self.col+self.ncols) and hit.getY() >= self.row and hit.getY() < (self.row+self.nrows):
				selhits.append(hit)
				if hit.getTime()<t0: t0=hit.getTime()
				pass
			pass
		prochits=0
		t1=9999
		while prochits<len(selhits):
			word = Word(self.col,self.row)
			word.setTime(t0)
			for hit in selhits:
				if hit.getTime()<t0+self.rpg_delay_ns:
					word.addHit(hit)
					prochits+=1
					pass
				else:
					if hit.getTime()>=t0 and hit.getTime()<t1: t1=hit.getTime()
				pass
			if word.getNHits()>0: self.words.append(word)
			t0=t1
			t1=9999
			pass
		pass
	pass
