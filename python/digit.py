#!/usr/bin/env python

from word import Word

class Digit():
	def __init__(self):
		# internal variables that define a digit
		self.refbit = 1
		self.pixel = 0
		self.group = 0
		self.parity = 0
		self.delay = 0
		self.dcolumn = 0
		# internal variables for pack/unpack
		self.bitstream = 0
		# decoded variables
		self.maltawords = [0,0]
		self.maltahits = []
		# truth variables
		self.words=[]
		self.time = 0
		pass
	def setTime(self, time):
		self.time=time
		pass
	def addWord(self, word):
		self.words.append(word)
		pass
	def getWords(self):
		return self.words
	def getNWords(self):
		return len(self.words)
	def getTime(self):
		return self.time
	def pack(self):
		self.pixel=0
		self.group=0
		self.dcolumn=0
		self.parity=0
		for word in self.words:
			self.pixel |= word.getBitStream()
			self.dcolumn |= word.getCol()>>1
			self.group |= word.getRow()>>4
			self.parity |= 1 if (word.getRow() % 16) > 7 else 0 
			pass
		# pack the malta words
		self.bitstream  = 0
		self.bitstream |= (self.refbit & 0x1)   << 0
		self.bitstream |= (self.pixel & 0xFFFF) << 1
		self.bitstream |= (self.group & 0x1F)   << 17
		self.bitstream |= (self.parity & 0x1)   << 22
		self.bitstream |= (self.delay & 0x7)    << 23
		self.bitstream |= (self.dcolumn & 0xFF) << 26
		pass
	def getBitStream(self):
		return self.bitstream
	def toStr(self):
		self.pack()
		return "dc=%2i,group=%2i,time=%5.2f,nwords=%i,pixel=0x%04x,0x%016X" % (self.dcolumn,self.group,self.time,len(self.words),self.pixel,self.bitstream)
	pass
		