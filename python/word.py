#!/usr/bin/env python

from hit import Hit

# This class describes the pixel address
# from the reference pulse generator
# it contains up to 16 hits
# column and row identifiers are the floor of the group position in the matrix

class Word():
	def __init__(self,col,row):
		self.hits=[]
		self.time = 0
		self.col = col
		self.row = row
		self.ncols = 2
		self.nrows = 8
		self.row_col_2_bit = [
			[0,8],
			[1,9],
			[2,10],
			[3,11],
			[4,12],
			[5,13],
			[6,14],
			[7,15],
		] 
		pass
	def setRow(self,row):
		self.row=row
		pass
	def setCol(self,col):
		self.col=col
		pass
	def setTime(self, time):
		self.time=time
		pass
	def addHit(self, hit):
		self.hits.append(hit)
		pass
	def getHits(self):
		return self.hits;
	def getNHits(self):
		return len(self.hits);
	def getRow(self):
		return self.row
	def getCol(self):
		return self.col
	def getTime(self):
		return self.time
	def getBitStream(self):
		bb=0
		for hit in self.hits:
			bb|=1<<self.row_col_2_bit[int(hit.getRow()%self.nrows)][int(hit.getCol()%self.ncols)]
			pass
		return bb
	def toStr(self):
		return "col=%3i,row=%3i,time=%5.2f,nhits=%2i,pixel_address=0x%04x" % (self.col,self.row,self.time,len(self.hits),self.getBitStream())
	pass
		