#!/usr/bin/env python
import random
import math
from hit import Hit

###################################################
# Model of the MALTA Analog Pixel
# This calculates the time to threshold 
# given a charge for a given threshold
#
# @Author Carlos.Solans@cern.ch
# @Date March 2021
####################################################
class Pixel():
	
	def __init__(self):
            
		#Look up table of the amount of table needed
		#to reach to a threshold of 230 electrons
		#extracted from Ivan's thesis
		#Each element is a point (x,y)
		#x is in kiloelectrons
		#y is in nanoseconds
		self.delayVsCharge=[
			[0.20,70.0], #0
			[0.22,50.0], #1
			[0.25,40.0], #2
			[0.40,20.0], #3
			[0.60,14.5], #4
			[1.00,10.0], #5
			[1.50,7.50], #6
			[2.00, 6.0], #7
			[2.60, 5.5], #8
			[3.00, 5.0], #9
		]
        
		# LUT to add a delay as a function of 
		# - radial distance to electrode 
		# - different dose levels: 0,1,2,3
		self.delayVsR={
			0: {"r":[0,10,20,22],"delay":[0.0,1.5, 4.0, 5.0]},
			1: {"r":[0,10,20,22],"delay":[0.0,1.5, 4.0, 5.0]},
			2: {"r":[0,10,20,22],"delay":[0.0,3.0,10.0,12.5]},
			3: {"r":[0,10,20,22],"delay":[0.0,3.5,15.0,17.5]}
		}
		
		# functional delay as a funcion of  
		# - radial distance to electrode 
		# - different dose levels: 0,1,2,3
		self.funcDelayVsR={
			0: [0.0000,0.0801,0.0064],
			1: [0.0000,0.0801,0.0064],
			2: [0.0000,0.0675,0.0223],
			3: [0.0000,0.0076,0.0363]
		}
		
		# Threshold value in electrons vs DACs
		# dac name : [[value, thr],...] 
		self.lutThr={
			"IDB"  :[[10,200],[20,230],[40,280],[60,300],[80,315],[100,330],[120,335]],
			"ITHR" :[[ 5,100],[20,160],[40,260],[60,335],[80,410],[100,480],[120,540]],
			"IBIAS":[[ 5,700],[10,500],[20,420],[35,355],[43,335],[52,330],[ 67,310],[85,300],[100,290]],
			"ICASN":[[ 5,380],[ 8,345],[10,335],[14,325],[ 17,325],[ 20,320]],
		}
		self.default_dacs={"IDB":120,"ITHR":60,"IBIAS":43,"ICASN":10}
		self.dacs = dict(self.default_dacs)
		
		#Internal propagation delay of the transistors in nanoseconds
		self.prop_delay=2

		#Threshold of the pixel in electrons
		self.threshold=335
		
		#Threshold noise
		self.threshold_rms=34

		#Pixel size col in um
		self.size_x = 36.4
		
		#Pixel size row in um
		self.size_y = 36.4
		
		#Dose in 1e15 neq/cm2
		self.dose = 0		
		
		#verbose flag
		self.verbose = False
		
		pass

	def setVerbose(self,enable):
		self.verbose=enable
		pass
		
	def getColSize(self):
		return self.size_x

	def getRowSize(self):
		return self.size_y

	def getDefaultDACs(self):
		return self.default_dacs
		
	def getDACs(self):
		return self.dacs
	
	def setIDB(self,idb):
		self.dacs["IDB"]=idb
		self.calculateThreshold()
		pass

	def setITHR(self,ithr):
		self.dacs["ITHR"]=ithr
		self.calculateThreshold()
		pass

	def setIBIAS(self,ibias):
		self.dacs["IBIAS"]=ibias
		self.calculateThreshold()
		pass

	def setICASN(self,icasn):
		self.dacs["ICASN"]=icasn
		self.calculateThreshold()
		pass
	
	def getIDB(self):
		return self.dacs["IDB"]
		
	def getITHR(self):
		return self.dacs["ITHR"]
	
	def getIBIAS(self):
		return self.dacs["IBIAS"]
	
	def getICASN(self):
		return self.dacs["ICASN"]
		
	def setThreshold(self, thr):
		self.threshold=thr
		pass

	def getThreshold(self):
		return self.threshold

	def setNoise(self, noise):
		self.threshold_rms=noise
		pass
	
	def getNoise(self):
		return self.threshold_rms

	def setDACs(self, idb, ithr, ibias, icasn):
		self.dacs["IDB"]=idb
		self.dacs["ITHR"]=ithr
		self.dacs["IBIAS"]=ibias
		self.dacs["ICASN"]=icasn
		self.calculateThreshold()
		pass
	
	def resetDACs(self):
		self.dacs["IDB"]=self.default_dacs["IDB"]
		self.dacs["ITHR"]=self.default_dacs["ITHR"]
		self.dacs["IBIAS"]=self.default_dacs["IBIAS"]
		self.dacs["ICASN"]=self.default_dacs["ICASN"]
		self.calculateThreshold()
		pass
		
	def calculateThreshold(self):
		t_idb  =self.interpolate_lut(self.lutThr["IDB"],  self.dacs["IDB"])
		t_ithr =self.interpolate_lut(self.lutThr["ITHR"], self.dacs["ITHR"])
		t_ibias=self.interpolate_lut(self.lutThr["IBIAS"],self.dacs["IBIAS"])
		t_icasn=self.interpolate_lut(self.lutThr["ICASN"],self.dacs["ICASN"])
		thr=math.sqrt((t_idb*t_idb)+(t_ithr*t_ithr)+(t_ibias*t_ibias)+(t_icasn*t_icasn))/math.sqrt(4)
		self.threshold=thr
		self.threshold_rms=thr/10
		if self.verbose: 
			s="calculateThreshold: "
			for dac in self.dacs: s+=" %s:%i," % (dac,self.dacs[dac])
			s+="=>%2.f" % thr
			print(s)
			pass
		pass
		
	def setDose(self, dose):
		if not dose in self.funcDelayVsR:
			print("Dose not recognized: %f" % dose)
			pass
		self.dose=dose
		pass
        
	def processStep(self,step):
		col,px=divmod(step.getX0(),self.size_x)
		row,py=divmod(step.getY0(),self.size_y)
		#convert to local coordinates: the electrode is at the center
		px-=self.size_x/2.
		py-=self.size_y/2.
		thr=random.gauss(self.threshold,self.threshold_rms)
		charge=step.getEdep()*1 # calibrate to electrons
		if charge<thr: return None
		ttt=self.calcDelayVsCharge(charge)
		ttt+=self.calcDelayVsR(px,py)
		hit=Hit(col,row,ttt+self.prop_delay)
		hit.setR(px,py)
		return hit
		
	def calcDelayVsCharge(self,charge):
		return self.interpolate_lut(self.delayVsCharge,charge/1000.)
	
	def calcDelayVsR(self,px,py):
		pr=math.sqrt(px*px+py*py)
		ttt=0
		for i in range(3):
			ttt+=self.funcDelayVsR[self.dose][i]*math.pow(pr,i)
			pass
		return ttt
        
	def interpolate_lut(self,ref,x):
		i1=0
		v1=10000
		i2=len(ref)-1
		v2=10000
		i=0
		for point in ref:
			if point[0]<=x and x-point[0]<v1:
				i1=i
				v1=x-point[0]
				pass
			if point[0]>=x and point[0]-x<v2:
				i2=i
				v2=point[0]-x
				pass
			i+=1
			pass
		if i2==0: i2=1
		if i1==len(ref)-1: i1=len(ref)-2
		x1=ref[i1][0]
		x2=ref[i2][0]
		y1=ref[i1][1]
		y2=ref[i2][1]
		#print("i1: %i, i2: %i, x1: %.2f, x2: %.2f, y1: %.2f, y2: %.2f" % (i1,i2,x1,x2,y1,y2))
		if i2==i1: y=y2
		else: y=y1+(x-x1)*(y2-y1)/(x2-x1)
		return y
	
	pass
      