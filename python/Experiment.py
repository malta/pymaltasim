#!/usr/bin/env python
import math
import random
import argparse
from step import Step
from malta import Malta

class Experiment():
	def __init__(self):
		self.verbose = False
		self.malta = Malta()
		self.steps = []
		self.digits = []
		pass
			
	def setVerbose(self, enable):
		self.verbose=enable
		self.malta.setVerbose(enable)
		pass
		
	def generateSteps(self, nsteps=1, mean_energy=300, rms_energy=20):
		if self.verbose: print("Generate steps: %i" % nsteps)
		max_x = self.malta.getPixel().getColSize()*self.malta.getNcols() #nm
		max_y = self.malta.getPixel().getRowSize()*self.malta.getNrows() #nm
		for i in range(nsteps):
			s=Step()
			s.setXYZ0(random.uniform(0,max_x),random.uniform(0,max_y),0)
			s.setEdep(random.gauss(mean_energy, rms_energy))
			if self.verbose: print("Step: %s"%s.toStr())
			self.steps.append(s)
			pass
		pass
	
	def addStep(self,step):
		self.steps.append(step)
		pass
        
	def getNumberOfSteps(self):
		return len(self.steps)

	def getNumberOfHits(self):
		return len(self.malta.getHits())
	
	def getNumberOfDigits(self):
		return len(self.malta.getDigits())

	def getNumberOfWords(self):
		return len(self.malta.getWords())

	def getSteps(self):
		return self.steps

	def getHits(self):
		return self.malta.getHits()
	
	def getDigits(self):
		return self.malta.getDigits()

	def getWords(self):
		return self.malta.getWords()
	
	def getMalta(self):
		return self.malta
        
	def startOfEvent(self):
		self.malta.processSteps(self.steps)
		pass
		
	def endOfEvent(self):
		self.malta.processHits()
		pass
	
	def clear(self):
		self.steps = []
		self.malta.clear()
		pass

	pass

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('-e', '--mean-energy', type=float, default=3000)
	parser.add_argument('-r', '--rms-energy', type=float, default=300)
	parser.add_argument('-n', '--num-events', type=int, default=1)
	parser.add_argument('-s', '--steps-per-event', type=int, default=20)
	args=parser.parse_args()
	experiment = Experiment()
	for i in range(args.num_events):
		print("Event number:     %i" % (i+1))
		experiment.generateSteps(args.steps_per_event, args.mean_energy, args.rms_energy)
		experiment.startOfEvent()
		experiment.endOfEvent()
		experiment.clear()
		pass
