#!/usr/bin/env python

#definition of a Hit

class Hit():
	def __init__(self, x=0, y=0, time=0):
		self.x=x
		self.y=y
		self.time=time
		self.rx=0
		self.ry=0
		pass
	def setX(self,x):
		self.x=x
		pass
	def setY(self,y):
		self.y=y
		pass
	def setTime(self,time):
		self.time=time
		pass
	def setRX(self,rx):
		self.rx=rx
		pass
	def setRY(self,ry):
		self.ry=ry
		pass
	def setR(self,rx,ry):
		self.rx=rx
		self.ry=ry
		pass
	def getX(self):
		return self.x
	def getY(self):
		return self.y
	def getRX(self):
		return self.rx
	def getRY(self):
		return self.ry
	def getTime(self):
		return self.time
	def getCol(self):
		return self.x
	def getRow(self):
		return self.y
	def toStr(self):
		s="col=%3i,row=%3i,rx=%5.2f,ry=%5.2f,time=%5.2f" % (self.x,self.y,self.rx,self.ry,self.time)
		return s
	pass
