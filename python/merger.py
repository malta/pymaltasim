#!/usr/bin/env python

from digit import Digit

#Merger circuit at the end of column of the MALTA matrix
#Any word that arrives closer than a predefined time will be merged
#Arrival time for a word will be calculated from the group row
#7ns/512 rows = 0.012367 ns/row 

class Merger():
	def __init__(self):
		self.digits=[]
		self.word_spacing_ns=1.6
		self.ns_per_row=0.0125
		self.verbose=False
		pass
	def setVerbose(self,enable):
		self.verbose=enable
		pass
	def setWordSpacing(self,ns):
		self.word_spacing_ns=ns
		pass
	def getDigits(self):
		return self.digits
	def clear(self):
		self.digits=[]
		pass
	def processWords(self, words):
		t0=1e6
		for word in words:
			teoc=word.getTime()+word.getRow()*self.ns_per_row
			if self.verbose: print("merger: teoc: %.2f, word: %s" % (teoc,word.toStr()))
			if teoc<t0: t0=teoc
			pass
		procwords=0
		t1=1e6
		while procwords<len(words):
			digit = Digit()
			digit.setTime(t0)
			for word in words:
				teoc=word.getTime()+word.getRow()*self.ns_per_row
				if teoc>=t0 and teoc<t0+self.word_spacing_ns:
					digit.addWord(word)
					procwords+=1
					pass
				else:
					if teoc>=t0+self.word_spacing_ns and teoc<t1: t1=teoc
				pass
			pass
			if digit.getNWords()>0: self.digits.append(digit)
			t0=t1
			t1=1e6
		pass