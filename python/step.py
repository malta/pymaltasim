#!/usr/bin/env python
#The step is the particle and it is bringing - charge, deps

class Step():
	def __init__(self):
		self.time = 0
		self.x0 = 0
		self.y0 = 0
		self.z0 = 0
		self.x1 = 0
		self.y1 = 0
		self.z1 = 0
		self.ekin = 0
		self.eDep = 0
		pass
	def setXYZ0(self, x0, y0, z0):
		self.x0 = x0
		self.y0 = y0
		self.z0 = z0
		pass
	def setXYZ1(self, x1, y1, z1):
		self.x1 = x1
		self.y1 = y1
		self.z1 = z1
		pass
	def getXYZ0(self):
		return [self.x0, self.y0, self.z0]
	def getXYZ1(self):
		return [self.x1, self.y1, self.z1]
	def getX0(self):
		return self.x0
	def getY0(self):
		return self.y0
	def getZ0(self):
		return self.z0
	def setTime(self, time):
		self.time=time
		pass
	def getTime(self):
		return self.time
		pass
	def setEdep(self, edep):
		self.eDep=edep
		pass
	def setEkin(self, ekin):
		self.eKin=ekin
		pass
	def getEdep(self):
		return self.eDep
	def getEkin(self):
		return self.eKin
	def toStr(self):
		return "x=%7.2fum,y=%7.2fum,z=%3.2f,eDep=%5.2f"%(self.x0,self.y0,self.z0,self.eDep)
	pass