#!/usr/bin/env python

from step import Step
from hit import Hit
from word import Word
from digit import Digit
from pixel import Pixel
from group import Group
from merger import Merger

class Malta():
	def __init__(self):
		self.verbose = False
		self.hits = []
		self.words = []
		self.digits = []
		self.pixel = Pixel()
		self.groups = []
		self.merger = Merger()
		self.ncols = 512
		self.nrows = 512
		row=0
		col=0
		while col<self.ncols:
			if False: print("create group at col=%i row=%i"%(col,row))
			group=Group(col,row)
			self.groups.append(group)
			row+=group.getNRows()
			if row>self.nrows:
				row=0
				col+=group.getNCols()
				pass
		pass
	def setVerbose(self,enable):
		self.verbose=enable
		self.pixel.setVerbose(enable)
		self.merger.setVerbose(enable)
		pass
	def getPixel(self):
		return self.pixel
	def getNrows(self):
		return self.nrows
	def getNcols(self):
		return self.ncols
	def getHits(self):
		return self.hits
	def getWords(self):
		return self.words
	def getDigits(self):
		return self.digits
	def clear(self):
		self.steps = []
		self.hits = []
		self.words = []
		self.digits = []
		self.merger.clear()
		for group in self.groups: group.clear()
		pass
	def processSteps(self,steps):
		#print("Number of steps: %i" % len(steps))
		for step in steps:
			hit=self.pixel.processStep(step)
			if hit: self.hits.append(hit)
			pass
		if self.verbose:
			print("Number of hits: %i" % len(self.hits))
			for hit in self.hits:
				print("hit: %s"%hit.toStr())
				pass
			pass
		pass
	def processHits(self):
		for group in self.groups:
			group.processHits(self.hits)
			for word in group.getWords():
				#print("word: %04x" % word.getBitStream())
				self.words.append(word)
				pass
			pass
		if self.verbose: 
			print("Number of words: %i" % len(self.words))
			for word in self.words:
				print("word: %s" % word.toStr())
				pass
			pass
		self.merger.processWords(self.words)
		self.digits=self.merger.getDigits()
		if self.verbose:
			print("Number of digits: %i" % len(self.digits))
			for digit in self.digits:
				print("digit: %s" % digit.toStr())
				pass
			pass
		pass
	pass

